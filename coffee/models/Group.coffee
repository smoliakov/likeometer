define [
  'backbone'
], (Backbone) ->
  class Group extends Backbone.Model
    defaults:
      src: ''
      link: ''
      title: ''
      size: 0

    initialize: (options) =>
      @set 'src', options.photo_200
      @set 'link', 'albums-' + options.id
      @set 'title', options.name

  class GroupCollection extends Backbone.Collection
    model: Group

  exports =
    Model: Group
    Collection: GroupCollection