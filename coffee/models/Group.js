// Generated by CoffeeScript 1.7.1
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['backbone'], function(Backbone) {
    var Group, GroupCollection, exports;
    Group = (function(_super) {
      __extends(Group, _super);

      function Group() {
        this.initialize = __bind(this.initialize, this);
        return Group.__super__.constructor.apply(this, arguments);
      }

      Group.prototype.defaults = {
        src: '',
        link: '',
        title: '',
        size: 0
      };

      Group.prototype.initialize = function(options) {
        this.set('src', options.photo_200);
        this.set('link', 'albums-' + options.id);
        return this.set('title', options.name);
      };

      return Group;

    })(Backbone.Model);
    GroupCollection = (function(_super) {
      __extends(GroupCollection, _super);

      function GroupCollection() {
        return GroupCollection.__super__.constructor.apply(this, arguments);
      }

      GroupCollection.prototype.model = Group;

      return GroupCollection;

    })(Backbone.Collection);
    return exports = {
      Model: Group,
      Collection: GroupCollection
    };
  });

}).call(this);
