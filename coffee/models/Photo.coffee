define [
  'backbone'
], (Backbone) ->
  class Photo extends Backbone.Model
	  initialize: (options) =>
		  sizes = ['x', 'm', 's', 'y']
		  photoCollection = new Backbone.Collection options.sizes
		  for size in sizes
			  photo = photoCollection.find (item) ->
				  item.get('type') is size
			  if photo
				  @set 'src', photo.get 'src'
				  break;

  class PhotoCollection extends Backbone.Collection
    model: Photo
    comparator: (photo1, photo2)->
      # sort by likes
      if photo1.get('likes').count < photo2.get('likes').count
        return 1
      if photo1.get('likes').count > photo2.get('likes').count
        return -1
      else
        # sort by comments
        if photo1.get('comments').count < photo2.get('comments').count
          return 1
        if photo1.get('comments').count > photo2.get('comments').count
          return -1
        else
          # sort by id
          if photo1.get('id') < photo2.get('id')
            return 1
          if photo1.get('id') > photo2.get('id')
            return -1
          return 0

  exports =
    Model: Photo
    Collection: PhotoCollection