define [
  'backbone'
], (Backbone) ->
  class Entity extends Backbone.Model
    defaults:
      title: ''
      thumb: ''
      size: ''

  Entity