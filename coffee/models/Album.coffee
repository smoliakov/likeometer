define [
	'backbone'
], (Backbone) ->
	class Album extends Backbone.Model
		initialize: (options) =>
			switch options.id
				when -6 then options.id = '0'
				when -7 then options.id = '00'
				when -15 then options.id = '000'

			sizes = ['x', 'm', 's', 'y']
			photoCollection = new Backbone.Collection options.sizes
			for size in sizes
				photo = photoCollection.find (item) ->
					item.get('type') is size
				if photo
					@set 'src', photo.get 'src'
					break;
			@set 'link', 'album' + options.owner_id + '_' + options.id
			@set 'title', options.title

	class AlbumCollection extends Backbone.Collection
		model: Album

	exports =
		Model: Album
		Collection: AlbumCollection