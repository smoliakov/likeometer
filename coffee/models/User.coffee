define [
  'backbone'
], (Backbone) ->
  class User extends Backbone.Model
    defaults:
      src: ''
      link: ''
      title: ''
	    counters:
        photos: 0
        groups: 0
        albums: 0
        friends: 0

    initialize: (options) =>
      @set 'src', options.photo_100
      @set 'link', 'albums' + options.id
      @set 'title', options.first_name + ' ' + options.last_name

  class UserCollection extends Backbone.Collection
    model: User

  exports =
    Model: User
    Collection: UserCollection