define [
	'jquery'
	'underscore'
	'backbone'
	'marionette'
	'coffee/Controller'
	'coffee/views/History'
	'coffee/views/Landing'
], ($
  _
  Backbone
  Marionette
  Controller
	HistoryView
	LandingView
) ->
	$('html').delegate 'a[href]:not([data-bypass])', 'click', (e)->
		e.preventDefault()
		App.router.navigate $(e.currentTarget).attr('href'), trigger: true

	$('.logout-link').click ->
		App.router.navigate "/logout", trigger: true

	App = new Backbone.Marionette.Application()
	window.App = App

	class Router extends Backbone.Marionette.AppRouter
		appRoutes:
			'(/)': 'index'
			'friends(/)': 'friends'
			'friends:id(/)': 'friends'
			'groups(/)': 'groups'
			'groups:id(/)': 'groups'
			'albums(/)': 'albums'
			'albums:id(/)': 'albums'
			'album:id(/)': 'album'
			'photo:id(/)': 'photo'
			'logout(/)': 'logout'
			'login(/)': 'login'
			'*anything': 'errorRoute'

		onRoute: =>
			VK.Auth.getLoginStatus (response) ->
				unless response.session
					App.router.navigate "/", trigger: true

	App.addRegions
		contentRegion: '.content'
		asideContentRegion: '.aside-content'
		photoSlideRegion: '#photo-g'
		landingRegion: '.app-landing'

	App.addInitializer ->
		console.log 'App started. Branch: master.'

		VK.Auth.getLoginStatus (response) ->
			if response.session
				VK.Api.call 'users.get', {fields: 'photo_50', v: 5.21}, (r) ->
					$('.app').removeClass('hide')
					$('.app-landing').addClass('hide')

					App.CURRENT_USER_ID = r.response[0].id

					user = r.response[0]

					$('.user .tab img').attr 'src', user.photo_50
					$('.user .tab span').text user.first_name + ' ' + user.last_name

					historyView = new HistoryView()
					App.asideContentRegion.show historyView
					App.likeHistory = historyView

					$('.curtains').hide()

			else
				App.router.navigate '/', trigger: true

		App.router = new Router
			controller: new Controller()

		Backbone.history.start
			pushState: true

	App