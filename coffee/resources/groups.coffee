define [
  'jquery',
  'backbone',
  'coffee/models/Group'
], ($, Backbone, GroupModel) ->
  class GroupsResource
    getCache = {}
    get: (userId) =>
      d = $.Deferred()
      params = new Backbone.Model
        extended: 1
        fields: 'counters'
        v: 5.4
      if userId
        params.set
          user_id: userId
      unless getCache[userId]
        VK.Api.call('groups.get', params.toJSON(), (response)->
          if response.error
            d.reject response.error.error_msg
          else
            getCache[userId] = response.response.items
            d.resolve new GroupModel.Collection response.response.items
        )
      else
        d.resolve new GroupModel.Collection getCache[userId]
      d.promise()

    getByIdCache = {}
    getById: (groupId) =>
      d = $.Deferred()
      groupId = Math.abs groupId
      params = new Backbone.Model
        group_ids: groupId
        fields: 'counters'
        v: 5.21
      unless getByIdCache[groupId]
        VK.Api.call('groups.getById', params.toJSON(), (response)->
          if response.error
            d.reject response.error.error_msg
          else
            getByIdCache[groupId] = response.response[0]
            d.resolve new GroupModel.Model response.response[0]
        )
      else
        d.resolve new GroupModel.Model getByIdCache[groupId]
      d.promise()

    VK.Observer.subscribe 'auth.logout', ->
      getCache = {}
      getByIdCache = {}

  GroupsResource