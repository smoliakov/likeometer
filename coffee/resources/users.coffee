define [
  'backbone',
  'underscore',
  'coffee/models/User'
], (Backbone, _, User) ->
  class Users
    getCache = {}
    get: (userId) ->
      d = $.Deferred()
      params = new Backbone.Model
        fields: 'photo_100,counters'
        v: 5.2
      if userId - 0
        params.set
          user_ids: userId
      unless getCache[userId]
        VK.Api.call('users.get', params.toJSON(), (response) =>
          if response.error
            d.reject response.error.error_msg
          else
            getCache[userId] = response.response[0]
            d.resolve new User.Model response.response[0]
        )
      else
        d.resolve new User.Model getCache[userId]
      d.promise()

    VK.Observer.subscribe 'auth.logout', ->
      getCache = {}

  Users