define [
	'jquery',
	'backbone',
	'underscore',
	'coffee/models/Album',
	'coffee/models/Photo'
], ($, Backbone, _, Album, Photo) ->
	class PhotosPart
		items: []

		constructor: (@offset, @key, @ownerId, @albumId) ->

		tryLoad: ->
			d = $.Deferred()
			params = new Backbone.Model
				extended: 1
				v: 5.21
				photo_sizes: 1
				owner_id: @ownerId
				album_id: @albumId
				offset: @offset
				count: 1000

			VK.Api.call 'photos.get', params.toJSON(), (response) =>
				if response.error
					console.log response.error
					d.reject()
				else
					d.resolve()
					@items = response.response.items

			d.promise()

	class PhotosResource
		getCache = {}
		get: (ownerId, albumId) ->
			d = $.Deferred()
			params = new Backbone.Model
				extended: 1
				v: 5.21
				photo_sizes: 1
				owner_id: ownerId
				album_id: albumId
			key = ownerId + '_' + albumId
			unless getCache[key]
				VK.Api.call 'photos.get', params.toJSON(), (response) =>
					if response.error
						d.reject response.error.error_msg
					else
						if response.response.count > response.response.items.length
							# load more items than 1000
							@_loadMore(key, ownerId, albumId, response.response.count)
							.done (items) =>
								items = items.concat response.response.items
								console.log 'Hola, items:', items.length
								getCache[key] = items
								d.resolve new Photo.Collection items
							.fail (error) =>
								d.reject error
						else
							# items count less than 1000
							getCache[key] = response.response.items
							d.resolve new Photo.Collection response.response.items
			else
				d.resolve new Photo.Collection getCache[key]
			d.promise()

		_loadMore: (key, ownerId, albumId, totalCount) ->
			dfd = $.Deferred()
			parts = []
			deferreds = []
			totalRequests = Math.floor totalCount / 1000
			for i in [0...totalRequests]
				offset = (i + 1) * 1000
				parts[i] = new PhotosPart(offset, key, ownerId, albumId)
				deferreds[i] = parts[i].tryLoad()

			watchDog = 0
			interval = setInterval ->
				watchDog++;
				if watchDog <= 120
					isComplete = true
					for i in [0...totalRequests]
						if deferreds[i].state() isnt 'resolved'
							isComplete = false
							break;
					if isComplete
						items = []
						for i in [0...totalRequests]
							items = items.concat parts[i].items
						clearInterval(interval)
						dfd.resolve items
					else
						for i in [0...totalRequests]
							if deferreds[i].state() is 'rejected'
								deferreds[i] = parts[i].tryLoad()
				else
					dfd.reject('Timeout. Ups.')
			, 1000

			dfd.promise()

		getAlbumsCache = {}
		getAlbums: (ownerId, albumId) ->
			ownerId -= 0
			albumId -= 0
			d = $.Deferred()
			params = new Backbone.Model
				need_covers: 1
				need_system: 1
				photo_sizes: 1
				album_ids: '-6,-7,-15'
				v: 5.21
			if ownerId
				params.set
					owner_id: ownerId
			if albumId
				params.set
					album_ids: albumId
			key = ownerId + '_' + albumId
			unless getAlbumsCache[key]
				VK.Api.call('photos.getAlbums', params.toJSON(), (response)->
					if response.error
						d.reject response.error.error_msg
					else
						albumCollection = new Album.Collection response.response.items
						if albumId
							albumsToResolve = albumCollection.findWhere
								id: albumId - 0
							if albumsToResolve
								getAlbumsCache[key] = albumsToResolve.toJSON()
								# return ONE album
								d.resolve albumsToResolve
							else
								d.reject()
						else
							getAlbumsCache[key] = response.response.items
							#return collection
							d.resolve new Album.Collection response.response.items
				)
			else
				if albumId
					d.resolve new Album.Model getAlbumsCache[key]
				else
					d.resolve new Album.Collection getAlbumsCache[key]
			d.promise()

		getByIdCache = {}
		getById: (ownerId, photoId)->
			ownerId -= 0
			photoId -= 0
			d = $.Deferred()
			params = new Backbone.Model
				v: 5.21
				extended: 1
				photo_sizes: 1
				photos: ownerId + '_' + photoId
			key = ownerId + '_' + photoId
			unless getByIdCache[key]
				VK.Api.call('photos.getById', params.toJSON(), (response)->
					if response.error
						d.reject response.error.error_msg
					else
						getByIdCache[key] = response.response[0]
						d.resolve new Photo.Model response.response[0]
				)
			else
				d.resolve new Photo.Model getByIdCache[key]
			d.promise()

		getSystemAlbumsCache = {}
		getSystemAlbums: (ownerId) ->
			ownerId -= 0
			d = $.Deferred()
			params = new Backbone.Model
				owner_id: ownerId
				v: 5.21
				album_ids: '-6,-7,-15'
				need_system: 1
				need_covers: 1
				photo_sizes: 1
			key = ownerId
			unless getSystemAlbumsCache[key]
				VK.Api.call 'photos.getAlbums', params.toJSON(), (response) ->
					if response.error
						d.reject response.error.error_msg
					else
						getSystemAlbumsCache[key] = response.response.items
						d.resolve new Album.Collection response.response.items
			else
				d.resolve new Album.Collection getSystemAlbumsCache[key]
			d.promise()

	VK.Observer.subscribe 'auth.logout', ->
		getCache = {}
		getAlbumsCache = {}
		getByIdCache = {}
		getSystemAlbumsCache = {}

	PhotosResource