define [
  'backbone',
  'underscore'
], (Backbone, _) ->
  class Storage
    set: (key, value) =>
      d = $.Deferred()
      params = new Backbone.Model
        key: key
        value: value
      VK.Api.call('storage.set', params.toJSON(), (response)->
        if response.error
          d.reject response.error.error_msg
        else
          d.resolve()
      )
      d.promise()

    get: (key) =>
      d = $.Deferred()
      VK.Api.call('storage.get', {key: key}, (response)->
        if response.error
          d.reject response.error.error_msg
        else
          d.resolve response
      )
      d.promise()

    push: (key, value) =>
      d = $.Deferred()
      @get(key)
        .done(
          (response) =>
            array = response.split ','
            array.push value
            @set(key, array)
              .done(
                d.resolve()
              )
              .fail(
                (error) =>
                  d.reject error
              )
        )
        .fail(
          (error) =>
            d.reject error
        )
      d.promise()

    without: (key, withoutValue) =>
      d = $.Deferred()
      @get(key)
        .done(
          (response) =>
            array = response.split ','
            array = _.without array, withoutValue
            @set(key, array)
              .done(
                d.resolve()
              )
              .fail(
                (error) =>
                  d.reject error
              )
        )
        .fail(
          (error) =>
            d.reject error
        )
      d.promise()

  Storage