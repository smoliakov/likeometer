define [
  'jquery',
  'backbone',
  'coffee/models/User'
], ($, Backbone, User) ->
  class LikesResource
    getListCache = {}
    getList: (ownerId, photoId, count) =>
      d = $.Deferred()
      params = new Backbone.Model
        owner_id: ownerId
        item_id: photoId
        type: 'photo'
        filter: 'likes'
        extended: 1
        count: count
        offset: 0
        v: 5.2
      key = ownerId + '_' + photoId
      unless getListCache[key]
        VK.Api.call('likes.getList', params.toJSON(), (response)->
          if response.error
            d.reject response.error.error_msg
          else
            getListCache[key] = response.response.items
            d.resolve new User.Collection response.response.items
        )
      else
        d.resolve new User.Collection getListCache[key]
      d.promise()

    VK.Observer.subscribe 'auth.logout', ->
      getListCache = {}

  LikesResource