define [
  'jquery',
  'backbone',
  'coffee/models/User'
], ($, Backbone, User) ->
  class Execute
    usersGet: (userId) =>
      d = $.Deferred()
      params = new Backbone.Model()
      if userId
        params.set
          user_id: userId
      VK.Api.call('execute.getCurrentUser', params.toJSON(), (response)->
        if response.error
          d.reject response.error.error_msg
        else
          d.resolve new User.Model response.response
      )
      d.promise()

    getUserDataCache = {}
#    getUserData: (userId) =>
#      params =
#        userId: if userId then userId else 'current'
#
#      d = $.Deferred()
#      unless getUserDataCache[userId]
#        VK.Api.call('execute.getDataForMainPage', params, (response)->
#          if response.error
#            d.reject response.error.error_msg
#          else
#            getUserDataCache[userId] = response.response
#            d.resolve response.response
#        )
#      else
#        console.log '[cached] UserDataCache'
#        d.resolve getUserDataCache[userId]
#      d.promise()

    getUserData:(userId) =>
      params =
        userId: if userId then userId else 'current'

      d = $.Deferred()
      unless getUserDataCache[userId]
        console.log '[vk] UserData'
        VK.Api.call('execute.getUserData', params, (response) ->
          if response.execute_errors
            errors = _.pluck response.execute_errors, 'error_code'
            if _.contains errors, 15
              d.reject 'Страница пользователя удалена. Информация недоступна.'
              return false
            else
              getUserDataCache[userId] = response.response
              d.resolve response.response
          else
            getUserDataCache[userId] = response.response
            d.resolve response.response
        )
      else
        console.log '[cached] UserData'
        d.resolve getUserDataCache[userId]
      d.promise()

    VK.Observer.subscribe 'auth.logout', ->
      getUserDataCache = null

  Execute