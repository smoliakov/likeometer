define [
  'jquery',
  'backbone',
  'coffee/models/User'
], ($, Backbone, User) ->
  class FriendCollection
    getCache = {}
    get: (userId) =>
      d = $.Deferred()
      params = new Backbone.Model
        fields: 'photo_100'
        order: 'name'
        v: 5.4
      if userId
        params.set
          user_id: userId
      unless getCache[userId]
        VK.Api.call('friends.get', params.toJSON(), (response)->
          if response.error
            d.reject response.error.error_msg
          else
            d.resolve new User.Collection response.response.items
        )
      else
        d.resolve new User.Collection getCache[userId]
      d.promise()

    VK.Observer.subscribe 'auth.logout', ->
      getCache = {}

  FriendCollection