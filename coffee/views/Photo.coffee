define [
	'backbone'
	'marionette'
	'coffee/models/Photo'
	'coffee/resources/photos'
	'text!templates/photo-slide.tpl'
], (
	Backbone
	Marionette
	Photo
	photos
	PhotoSlideTemplate
) ->
	class PhotoSlideView extends Backbone.Marionette.Layout
		template: _.template PhotoSlideTemplate
		className: 'photo-g'

		events:
			'click .next': 'onNextClick'
			'click .previous': 'onPreviousClick'
			'click .close': 'onCloseClick'

		ui:
			photo: '#photo-slide'
			counter: '.counter'
			likes: '.likes .fa-heart'
			download: '.download'
			external: '.external'

		initialize: =>
			@state = new Backbone.Model @options
			@state.on 'change', =>
				@update()

			@photo = new Backbone.Model()
			@photos = if @options.collection then @options.collection else (new Backbone.Collection())

		update: =>
			@ui.photo.addClass('slow-hide')

			# find photo
			@photo = @photos.find (item) =>
				item.get('id') is @state.get('photoId')

			# set counter
			@index = @photos.indexOf @photo
			@ui.counter.text "#{@index + 1} из #{@photos.length}"

			# set likes
			@ui.likes.text @photo.get('likes').count
			if @photo.get('likes').user_likes
				@ui.likes.addClass('gold-before')
			else
				@ui.likes.removeClass('gold-before')

			# set photo
			for size in ['w', 'z', 'y', 'x', 'q', 'p', 'o', 'm', 's']
				data = _.find @photo.get('sizes'), (item) ->
					item.type is size
				break if data

			# set download and external
			@ui.download.attr 'href', data.src
			@ui.external.attr 'href', "//vk.com/photo#{@photo.get('owner_id')}_#{@photo.get('id')}"

			img = new Image()
			img.onload = =>
				@ui.photo.attr('src', data.src).removeClass('slow-hide')
				@resize()
			img.src = data.src

		resize: =>
			$photo = @ui.photo
			$window = $(window)

			if $photo.height() < $window.height()
				marginTop = ($window.height() - $photo.height()) / 2
				$photo.css 'margin-top', marginTop
			else
				$photo.css 'margin-top', 0

		onRender: =>
			debouncedResize = _.debounce(@resize, 100)
			$(window).resize(debouncedResize).keyup(@onWindowKeyup)

			photosResource = new photos()
			unless @options.collection
				photosResource.getById(@state.get('ownerId'), @state.get('photoId'))
				.then (photo) =>
					albumId = photo.get('album_id')
					photosResource.get(@state.get('ownerId'), albumId)
					.then (@photos) =>
						@update()
					.fail (error) =>
						console.log error
				.fail (error) =>
					console.log error
			else
				@update()

		onBeforeClose: =>
			$(window).unbind('resize').unbind('keyup')
			$('body').removeClass 'photo-open'

		onNextClick: =>
			nextPhoto = @photos.at @index + 1
			unless nextPhoto
				@index = 0
				nextPhoto = @photos.at @index
			url = "/photo#{nextPhoto.get('owner_id')}_#{nextPhoto.get('id')}"
			App.router.navigate url, trigger: true

		onPreviousClick: =>
			previousPhoto = @photos.at @index - 1
			unless previousPhoto
				@index = @photos.length - 1
				previousPhoto = @photos.at @index
			url = "/photo#{previousPhoto.get('owner_id')}_#{previousPhoto.get('id')}"
			App.router.navigate url, trigger: true

		onCloseClick: =>
			albumId = switch @photo.get('album_id')
				when -6 then '0'
				when -7 then '00'
				when -15 then '000'
				else @photo.get('album_id')

			url = "/album#{@photo.get('owner_id')}_#{albumId}"
			App.router.navigate url, trigger: true

		onWindowKeyup: (e) =>
			key = e.keyCode || e.which
			@onCloseClick() if key is 27

	PhotoSlideView