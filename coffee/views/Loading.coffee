define ['marionette', 'text!templates/loading.tpl'], (Marionette, LoadingTemplate) ->
	class Loading extends Backbone.Marionette.ItemView
		className: 'content-loader'
		template: _.template LoadingTemplate

	Loading