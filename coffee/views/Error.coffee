define ['marionette', 'text!templates/error.tpl'], (Marionette, ErrorTemplate) ->
	class ErrorView extends Backbone.Marionette.ItemView
		className: 'text-center error'
		template: _.template ErrorTemplate

		initialize: =>
			defaults =
				title: 'Упс, что-то пошло не так.'
				message: ''

			@model = new Backbone.Model _.extend defaults, @options

	ErrorView