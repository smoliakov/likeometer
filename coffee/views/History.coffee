define ['marionette', 'text!templates/history.tpl'], (Marionette, HistoryItemTemplate) ->
	class HistoryItem extends Marionette.ItemView
		template: _.template HistoryItemTemplate
		className: 'tab'
		tagName: 'a'

		initialize: =>
			@$el.attr 'href', @model.get('link')

	class HistoryCollection extends Backbone.Collection
		comparator: (item1, item2) ->
			if item1.get('time') > item2.get('time')
				return 1
			if item1.get('time') < item2.get('time')
				return -1

	class History extends Backbone.Marionette.CollectionView
		className: 'history'
		itemView: HistoryItem

		initialize: =>
			if typeof(Storage) isnt "undefined" and localStorage.getItem('historyCollection')
				historyItems = []
				try
					historyItems = JSON.parse localStorage.getItem('historyCollection')
				catch
					console.log 'Some shit in localStorage.'
					localStorage.removeItem('historyCollection')
				@collection = new HistoryCollection historyItems, sort: false
			else
				@collection = new HistoryCollection()

		add: (item) =>
			if item.title
				searchItem = @collection.find (i) ->
					i.get('link') is item.link
				if searchItem
					@collection.remove searchItem
					@collection.unshift item
				else
					if @collection.length >= 50
						@collection.pop()
					@collection.unshift item

				@addToLocalStorage()

		appendHtml: (collectionView, itemView, index) ->
			if collectionView.isBuffering
				collectionView.elBuffer.appendChild(itemView.el)
			else
				collectionView.$el.prepend(itemView.el)

		addToLocalStorage: ->
			if typeof(Storage) isnt "undefined"
				localStorage.setItem "historyCollection", JSON.stringify @collection.toJSON()


	History