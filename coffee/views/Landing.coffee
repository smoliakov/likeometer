define ['marionette', 'text!templates/landing.tpl'], (Marionette, LandingTemplate) ->
	class Landing extends Backbone.Marionette.ItemView
		className: 'landing'
		template: _.template LandingTemplate
		attributes:
			'data-stellar-background-ratio': "0.5"