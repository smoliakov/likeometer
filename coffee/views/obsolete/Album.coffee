define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/image-item-album.tpl',
  'text!templates/content.tpl'
], (Backbone, Marionette, _, ImageItemTemplate, ContentTemplate) ->
  class AlbumItemView extends Backbone.Marionette.ItemView
    template: _.template ImageItemTemplate
    className: 'cell'

    initialize: (options)=>
      id = @model.get('id')
      switch id
        when -15 then id = '000'
        when -7 then id = '00'
        when -6 then id = '0'
      @model.set id: id

  class AlbumCollectionView extends Backbone.Marionette.CompositeView
    itemView: AlbumItemView
    template: _.template ContentTemplate

  exports =
    ItemView: AlbumItemView
    CompositeView: AlbumCollectionView