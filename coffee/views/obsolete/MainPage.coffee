define [
  '../../../js/underscore-min'
  'backbone'
  'marionette'
  'coffee/models/User'
  'coffee/models/Album'
  'coffee/models/Group'
  'coffee/models/Entity'
  'coffee/resources/execute'
  'coffee/views/EntityView'
  'coffee/views/EmptyView'
  'text!templates/main-albums.tpl'
], (
  _
  Backbone
  Marionette
  User
  Album
  Group
  Entity
  execute
  EntityView
  EmptyView
  Template
) ->
  class MainPage extends Backbone.Marionette.Layout
    template: _.template Template
    className: 'content'

    regions:
      albums: '#myAlbums'
      groups: '#myGroups'
      friends: '#myFriends'

    onRender: =>
      (new execute()).getUserData(@options.userId)
        .done(
          (response) =>
            if @options.userId
              # if we are having a user
              userName = response.user.first_name # + ' ' + response.user.last_name
              albumsEntity = new Entity
                title: 'Альбомы ' + userName
                thumb: response.user.photo_50
              groupsEntity = new Entity title: 'Группы ' + userName
              friendsEntity = new Entity title: 'Друзья ' + userName
              window.document.title = response.userNom.first_name + ' ' + response.userNom.last_name
            else
              # current user
              albumsEntity = new Entity title: 'Мои альбомы'
              groupsEntity = new Entity title: 'Мои группы'
              friendsEntity = new Entity title: 'Мои друзья'

            if response.albums and response.albums.length > 0
              albumCollectionView = new EntityView
                collection: new Album.Collection response.albums
                model: albumsEntity
                className: ''
              @albums.show albumCollectionView

            if response.groups and response.groups.length > 0
              groupsPage = new EntityView
                collection: new Group.Collection response.groups
                model: groupsEntity
                className: ''
              @groups.show groupsPage

            if response.friends and response.friends.length > 0
              friendsPage = new EntityView
                collection: new User.Collection response.friends
                model: friendsEntity
                className: ''
              @friends.show friendsPage
        )
        .fail(
          (error) =>
            console.log '[error]', error
            App.content.show new EmptyView
              errorText: error
        )

  MainPage