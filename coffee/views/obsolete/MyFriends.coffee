define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/albums-item.tpl',
  'text!templates/content.tpl'
], (Backbone, Marionette, _, ItemTemplate, ContentTemplate) ->
  class FriendItemView extends Backbone.Marionette.ItemView
    template: _.template ItemTemplate
    className: 'cell'

  class FriendCollectionView extends Backbone.Marionette.CompositeView
    itemView: FriendItemView
    template: _.template ContentTemplate
    className: 'content'

  FriendCollectionView