define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/previous.tpl'
], (Backbone, Marionette, _, PreviousViewTemplate) ->
  class PreviousItemView extends Backbone.Marionette.ItemView
    template: _.template PreviousViewTemplate
    className: 'prev-page'

    initialize: =>
      @model = new Backbone.Model @options

    onRender: =>
      @$el.click =>
        AppRouter.navigate @options.url, {trigger: true}

  PreviousItemView