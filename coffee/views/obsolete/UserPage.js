// Generated by CoffeeScript 1.7.1
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['../../../js/backbone-min', 'marionette', 'underscore', 'text!templates/image-item-album.tpl', 'text!templates/user-content.tpl'], function(Backbone, Marionette, _, ImageItemTemplate, ContentTemplate) {
    var AlbumItemView, UserCollectionView;
    AlbumItemView = (function(_super) {
      __extends(AlbumItemView, _super);

      function AlbumItemView() {
        this.initialize = __bind(this.initialize, this);
        return AlbumItemView.__super__.constructor.apply(this, arguments);
      }

      AlbumItemView.prototype.template = _.template(ImageItemTemplate);

      AlbumItemView.prototype.className = 'cell';

      AlbumItemView.prototype.initialize = function() {
        var aid;
        aid = this.model.get('aid');
        switch (aid) {
          case -15:
            aid = '000';
            break;
          case -7:
            aid = '00';
            break;
          case -6:
            aid = '0';
        }
        return this.model.set({
          aid: aid
        });
      };

      return AlbumItemView;

    })(Backbone.Marionette.ItemView);
    UserCollectionView = (function(_super) {
      __extends(UserCollectionView, _super);

      function UserCollectionView() {
        return UserCollectionView.__super__.constructor.apply(this, arguments);
      }

      UserCollectionView.prototype.itemView = AlbumItemView;

      UserCollectionView.prototype.template = _.template(ContentTemplate);

      UserCollectionView.prototype.className = 'content';

      return UserCollectionView;

    })(Backbone.Marionette.CompositeView);
    return UserCollectionView;
  });

}).call(this);
