// Generated by CoffeeScript 1.7.1
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['../../../js/backbone-min', 'marionette', 'underscore', 'coffee/models/Photo', 'coffee/resources/photos', 'text!templates/photo-page.tpl'], function(Backbone, Marionette, _, Photo, photos, PhotoPageTemplate) {
    var PhotoItemView;
    PhotoItemView = (function(_super) {
      __extends(PhotoItemView, _super);

      function PhotoItemView() {
        this.showPhoto = __bind(this.showPhoto, this);
        this.onNextButtonClick = __bind(this.onNextButtonClick, this);
        this.onPrevButtonClick = __bind(this.onPrevButtonClick, this);
        this.centerImage = __bind(this.centerImage, this);
        this.onClose = __bind(this.onClose, this);
        this.onCloseClick = __bind(this.onCloseClick, this);
        this.onMakeCoverClick = __bind(this.onMakeCoverClick, this);
        this.onRender = __bind(this.onRender, this);
        this.initialize = __bind(this.initialize, this);
        this.onDownloadClick = __bind(this.onDownloadClick, this);
        return PhotoItemView.__super__.constructor.apply(this, arguments);
      }

      PhotoItemView.prototype.template = _.template(PhotoPageTemplate);

      PhotoItemView.prototype.className = 'photo';

      PhotoItemView.prototype.ui = {
        closeButton: '.close',
        photo: '#currentImage',
        likes: '.photo-likes',
        comments: '.photo-comments',
        nextButton: '.right-arrow-wrapper',
        prevButton: '.left-arrow-wrapper',
        text: '.text',
        count: '.count',
        downloadButton: '.download a',
        loader: '.image-wrapper .loader'
      };

      PhotoItemView.prototype.events = {
        'click .close': 'onCloseClick',
        'click .left-arrow-wrapper': 'onPrevButtonClick',
        'click .right-arrow-wrapper': 'onNextButtonClick',
        'click #currentImage': 'onNextButtonClick',
        'click .download a': 'onDownloadClick'
      };

      PhotoItemView.prototype.onDownloadClick = function() {
        return console.log('CLICK');
      };

      PhotoItemView.prototype.initialize = function(options) {
        var centerImageDebounced;
        if (options.photoCollection) {
          options.photoCollection = new Photo.Collection(options.photoCollection);
        }
        $(document).bind('keydown', (function(_this) {
          return function(e) {
            switch (e.keyCode) {
              case 27:
                return _this.onCloseClick();
              case 39:
                return _this.onNextButtonClick();
              case 37:
                return _this.onPrevButtonClick();
            }
          };
        })(this));
        $(document).bind('mousewheel', function(e) {
          e.preventDefault();
          return false;
        });
        centerImageDebounced = _.debounce(this.centerImage, 300);
        window.onresize = centerImageDebounced;
        $(window).resize((function(_this) {
          return function() {
            return centerImageDebounced();
          };
        })(this));
        return this.options = options;
      };

      PhotoItemView.prototype.onRender = function() {
        var photosResource;
        if (!$('body').hasClass('galleryMode')) {
          $('body').addClass('galleryMode');
        }
        if (!this.options.photoCollection) {
          photosResource = new photos();
          return photosResource.getById(this.options.ownerId, this.options.photoId).done((function(_this) {
            return function(currentPhotoResponse) {
              _this.model = currentPhotoResponse;
              return photosResource.get(_this.model.get('owner_id'), _this.model.get('album_id')).done(function(photoCollectionResponse) {
                _this.photoCollection = photoCollectionResponse.clone();
                _this.index = _this.photoCollection.pluck('id').indexOf(_this.options.photoId);
                return _this.showPhoto();
              }).fail(function(error) {
                return console.log('Error photo page', error);
              });
            };
          })(this)).fail((function(_this) {
            return function(error) {
              return console.log('Error photo page', error);
            };
          })(this));
        } else {
          this.photoCollection = this.options.photoCollection;
          this.index = this.photoCollection.pluck('id').indexOf(this.options.photoId);
          this.model = this.photoCollection.at(this.index);
          return this.showPhoto();
        }
      };

      PhotoItemView.prototype.onMakeCoverClick = function() {
        return console.log('onMakeCoverClick');
      };

      PhotoItemView.prototype.onCloseClick = function() {
        var albumId, href;
        albumId = (function() {
          switch (this.model.get('album_id')) {
            case -6:
              return '0';
            case -7:
              return '00';
            case -15:
              return '000';
            default:
              return this.model.get('album_id');
          }
        }).call(this);
        href = 'album' + this.model.get('owner_id') + '_' + albumId;
        if ($('#content').html()) {
          window.AppRouter.navigate(href, {
            trigger: false
          });
        } else {
          window.AppRouter.navigate(href, {
            trigger: true
          });
        }
        return this.close();
      };

      PhotoItemView.prototype.onClose = function() {
        $(document).unbind('keydown');
        $(document).unbind('mousewheel');
        return $('body').removeClass('galleryMode');
      };

      PhotoItemView.prototype.centerImage = function() {
        var h, left, top, w;
        this.bindUIElements();
        h = $(window).height();
        w = $(window).width();
        top = (h - this.ui.photo.height()) / 2;
        left = (w - this.ui.photo.width()) / 2;
        return this.ui.photo.css({
          top: top + 'px',
          left: left + 'px'
        });
      };

      PhotoItemView.prototype.onPrevButtonClick = function() {
        this.index--;
        if (this.index >= 0) {
          this.model = this.photoCollection.at(this.index);
          return this.showPhoto();
        } else {
          return this.index = 0;
        }
      };

      PhotoItemView.prototype.onNextButtonClick = function() {
        this.index++;
        if (this.index < this.photoCollection.length) {
          this.model = this.photoCollection.at(this.index);
          return this.showPhoto();
        } else {
          return this.index--;
        }
      };

      PhotoItemView.prototype.showPhoto = function() {
        var href, image, photo, photoSizes, pid, src, _i, _len;
        href = 'photo' + this.model.get('owner_id') + '_' + this.model.get('id');
        window.AppRouter.navigate(href, {
          trigger: false
        });
        photoSizes = ['photo_75', 'photo_130', 'photo_604', 'photo_807', 'photo_1280', 'photo_2560'].reverse();
        src = null;
        for (_i = 0, _len = photoSizes.length; _i < _len; _i++) {
          photo = photoSizes[_i];
          if (this.model.get(photo)) {
            src = this.model.get(photo);
            break;
          }
        }
        this.ui.loader.css('opacity', 1);
        image = new Image();
        image.src = src;
        pid = this.model.get('id');
        image.onload = (function(_this) {
          return function() {
            if (pid === _this.ui.photo.data('pid')) {
              _this.ui.photo.attr('src', src);
              _this.centerImage();
              return _this.ui.loader.css('opacity', 0);
            }
          };
        })(this);
        this.ui.photo.data('pid', this.model.get('id'));
        this.ui.likes.text(this.model.get('likes').count);
        this.ui.comments.text(this.model.get('comments').count);
        this.ui.text.html(_.escape(this.model.get('text')));
        this.ui.count.text(this.index + 1 + ' из ' + this.photoCollection.length);
        this.ui.downloadButton.attr({
          href: src
        });
        if (this.model.get('likes').user_likes > 0) {
          return this.ui.likes.addClass('gold');
        } else {
          return this.ui.likes.removeClass('gold');
        }
      };

      return PhotoItemView;

    })(Backbone.Marionette.ItemView);
    return PhotoItemView;
  });

}).call(this);
