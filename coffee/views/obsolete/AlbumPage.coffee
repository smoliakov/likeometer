define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'coffee/models/Photo',
  'text!templates/photo-albums-item.tpl',
  'text!templates/album-content.tpl'
], (Backbone, Marionette, _, Photo, PhotoItemTemplate, ContentTemplate) ->
  class AlbumItemView extends Backbone.Marionette.ItemView
    template: _.template PhotoItemTemplate
    className: 'cell'

  class AlbumCollectionView extends Backbone.Marionette.CompositeView
    itemView: AlbumItemView
    template: _.template ContentTemplate
    className: 'content'

    collectionCache = null

    ui:
      photos: '.filter .photos-count'
      likes: '.filter .likes'
      userLikes: '.filter .user-likes'
      comments: '.filter .comments'

    events:
      'click #filterAllPhotos': 'onFilterAllPhotosClick'
      'click #filterUserLikes': 'onFilterUserLikes'
      'click #filterLikes': 'onFilterLikes'
      'click #filterComments': 'onFilterComments'

      'click #sortByDate': 'onSortByDateClick'
      'click #sortByLikes': 'onSortByLikesClick'
      'click #sortByComments': 'onSortByCommentsClick'


    initialize: (options)=>
      collectionCache = @collection.clone()

      photosInViewPort = Math.ceil ($(window).height() - 85) * $(window).width() / 8500
      @scroll =
        offset: 0
        count: photosInViewPort * 2
        canScroll: true

      @collection.reset collectionCache.slice @scroll.offset, @scroll.count
#      console.log 'log', collectionCache.length, @collection.length

    onFilterAllPhotosClick: (e) =>
      @setSelected e
      @collection.reset collectionCache

    onFilterUserLikes: (e) =>
      @setSelected e
      filteredItems = new Photo.Collection(collectionCache).filter (item) ->
        item.get('likes').user_likes
      @collection.reset filteredItems

    onFilterLikes: (e) =>
      @setSelected e
      filteredItems = new Photo.Collection(collectionCache).filter (item) ->
        item.get('likes').count > 0
      @collection.reset filteredItems

    onFilterComments: (e) =>
      @setSelected e
      filteredItems = new Photo.Collection(collectionCache).filter (item) ->
        item.get('comments').count > 0
      @collection.reset filteredItems

    onSortByDateClick: =>
      sortedCollection = @collection.sortBy (item)->
        -item.get('id')
      @collection.reset sortedCollection, {sort: false}

    onSortByLikesClick: =>
      sortedCollection = @collection.sortBy (item)->
        -item.get('likes').count
      @collection.reset sortedCollection, {sort: false}

    onSortByCommentsClick: =>
      sortedCollection = @collection.sortBy (item)->
        -item.get('comments').count
      @collection.reset sortedCollection, {sort: false}

    setSelected: (e)=>
      $('li').removeClass('selected')
      $(e.currentTarget).addClass('selected')

    onRender: =>
      likes = 0
      userLikes = 0
      comments = 0

      @collection.each (item) ->
        comments += item.get('comments').count
        likes += item.get('likes').count
        userLikes += item.get('likes').user_likes

      @ui.photos.after @collection.length
      @ui.likes.after likes
      @ui.userLikes.after userLikes
      @ui.comments.after comments

      win = $(window)
      win.bind 'scroll', =>
        if (win.scrollTop() + win.height() is $(document).height()) and @scroll.canScroll
          if @scroll.offset > collectionCache.length
            @scroll.canScroll = false
          @scroll.offset += @scroll.count
          @collection.add collectionCache.slice @scroll.offset, @scroll.count + @scroll.offset

    onBeforeClose: ->
      $(window).unbind 'scroll'

  AlbumCollectionView

#  function getParameterByName(name) {
#  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
#  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
#    results = regex.exec(location.search);
#return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
#}