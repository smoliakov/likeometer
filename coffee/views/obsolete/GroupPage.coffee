define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/image-item-album.tpl',
  'text!templates/group-content.tpl'
], (Backbone, Marionette, _, ImageItemTemplate, ContentTemplate) ->
  class AlbumItemView extends Backbone.Marionette.ItemView
    template: _.template ImageItemTemplate
    className: 'cell'

    initialize: =>
      aid = @model.get 'aid'
      switch aid
        when -15 then aid = '000'
        when -7 then aid = '00'
        when -6 then aid = '0'
      @model.set aid: aid

  class AlbumCollectionView extends Backbone.Marionette.CompositeView
    itemView: AlbumItemView
    template: _.template ContentTemplate
    className: 'content'

  AlbumCollectionView