define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'coffee/models/Photo',
  'coffee/resources/photos',
  'text!templates/photo-page.tpl'
], (Backbone, Marionette, _, Photo, photos, PhotoPageTemplate) ->
  class PhotoItemView extends Backbone.Marionette.ItemView
    template: _.template PhotoPageTemplate
    className: 'photo'

    ui:
      closeButton: '.close'
      photo: '#currentImage'
      likes: '.photo-likes'
      comments: '.photo-comments'
      nextButton: '.right-arrow-wrapper'
      prevButton: '.left-arrow-wrapper'
      text: '.text'
      count: '.count'
      downloadButton: '.download a'
      loader: '.image-wrapper .loader'

    events:
      'click .close': 'onCloseClick'
      'click .left-arrow-wrapper': 'onPrevButtonClick'
      'click .right-arrow-wrapper': 'onNextButtonClick'
      'click #currentImage': 'onNextButtonClick'
#      'click .make-cover': 'onMakeCoverClick'
      'click .download a': 'onDownloadClick'

    onDownloadClick: =>
      console.log 'CLICK'

    initialize: (options)=>
      if options.photoCollection
        options.photoCollection = new Photo.Collection options.photoCollection

      $(document).bind 'keydown', (e)=>
#        console.log e.keyCode
        switch e.keyCode
          when 27 then @onCloseClick()       # esc
          when 39 then @onNextButtonClick()  # right arrow
          when 37 then @onPrevButtonClick()  # left arrow

      $(document).bind 'mousewheel', (e)->
        e.preventDefault()
        false

      centerImageDebounced = _.debounce @centerImage, 300
      window.onresize = centerImageDebounced
      $(window).resize =>
        centerImageDebounced()

      @options = options

    onRender: =>
      unless $('body').hasClass('galleryMode')
        $('body').addClass('galleryMode')

      unless @options.photoCollection
        photosResource = new photos()
        photosResource.getById(@options.ownerId, @options.photoId)
          .done(
            (currentPhotoResponse)=>
              @model = currentPhotoResponse
              photosResource.get(@model.get('owner_id'), @model.get('album_id'))
                .done(
                  (photoCollectionResponse)=>
                    @photoCollection = photoCollectionResponse.clone()
                    @index = @photoCollection.pluck('id').indexOf(@options.photoId)
                    @showPhoto()
                )
                .fail(
                  (error)=>
                    console.log 'Error photo page', error
                )
          )
          .fail(
            (error)=>
              console.log 'Error photo page', error
          )
      else
        @photoCollection = @options.photoCollection
        @index = @photoCollection.pluck('id').indexOf(@options.photoId)
        @model = @photoCollection.at(@index)
        @showPhoto()

    onMakeCoverClick: =>
      console.log 'onMakeCoverClick'

    onCloseClick: =>
      albumId = switch (@model.get('album_id'))
        when -6 then '0'
        when -7 then '00'
        when -15 then '000'
        else @model.get('album_id')
      href = 'album' + @model.get('owner_id') + '_' + albumId
      if $('#content').html()
        window.AppRouter.navigate href, {trigger: false}
      else
        window.AppRouter.navigate href, {trigger: true}
      @close()

    onClose: =>
      $(document).unbind 'keydown'
      $(document).unbind 'mousewheel'
      $('body').removeClass 'galleryMode'

    centerImage: =>
      @bindUIElements()
      h = $(window).height()
      w = $(window).width()
      top = (h - @ui.photo.height()) / 2;
      left = (w - @ui.photo.width()) / 2;
      @ui.photo.css
        top: top + 'px',
        left: left + 'px'

    onPrevButtonClick: =>
      @index--
      if @index >= 0
        @model = @photoCollection.at(@index)
        @showPhoto()
      else
        @index = 0

    onNextButtonClick: =>
      @index++
      if @index < @photoCollection.length
        @model = @photoCollection.at(@index)
        @showPhoto()
      else
        @index--

    showPhoto: =>
      href = 'photo' + @model.get('owner_id') + '_' + @model.get('id')
      window.AppRouter.navigate href, {trigger: false}

      photoSizes = ['photo_75', 'photo_130', 'photo_604', 'photo_807', 'photo_1280', 'photo_2560'].reverse()
      src = null
      for photo in photoSizes
        if @model.get(photo)
          src = @model.get(photo)
          break

      @ui.loader.css('opacity', 1)
      image = new Image()
      image.src = src
      pid = @model.get('id')
      image.onload = =>
#        unless @isClosed
        if pid is @ui.photo.data('pid')
          @ui.photo.attr('src', src)
          @centerImage()
          @ui.loader.css('opacity', 0)

      @ui.photo.data 'pid', @model.get('id')
      @ui.likes.text @model.get('likes').count
      @ui.comments.text @model.get('comments').count
      @ui.text.html _.escape @model.get('text')
      @ui.count.text(@index + 1 + ' из ' + @photoCollection.length)
      @ui.downloadButton.attr href: src

      if @model.get('likes').user_likes > 0
        @ui.likes.addClass('gold')
      else
        @ui.likes.removeClass('gold')


  PhotoItemView