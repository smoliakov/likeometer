define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/entity.tpl', # page layout template
  'text!templates/albums-item.tpl'    # item layout template
], (Backbone, Marionette, _, EntityTemplate, ItemTemplate) ->
  class ItemView extends Backbone.Marionette.ItemView
    template: _.template ItemTemplate
    className: 'cell'

  class EntityView extends Backbone.Marionette.CompositeView
    itemView: ItemView
    itemViewContainer: '.items'
    template: _.template EntityTemplate
    className: 'content'

    initialize: =>
      @model.set 'size', @collection.length

  EntityView