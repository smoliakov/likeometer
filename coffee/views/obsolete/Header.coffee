define [
  '../../../js/jquery-1.10.2.min',
  'backbone',
  'marionette',
  'underscore',
  'coffee/resources/users',
  'coffee/resources/execute',
  'text!templates/header.tpl'
], ($, Backbone, Marionette, _, users, execute, HeaderTemplate) ->
  class CurrentUserItemView extends Backbone.Marionette.ItemView
    template: _.template HeaderTemplate
    className: 'header'

    ui:
      header: '.header'
      userName: '#userName'
      userAlbumsCount: '#userAlbumsCount'
      userPic: '#userPic'
      userData: '.user-data'

      dropdownMenu: '.dropdown-header'
      dropdownMyFriends: '#dropdown-my-friends'
      dropdownMyGroups: '#dropdown-my-groups'
      dropdownMyPhotos: '#dropdown-my-photos'
      dropdownExit: '#dropdown-exit'

    onRender: =>
      (new execute()).usersGet()
        .done(
          (currentUser) =>
            @ui.userName.text currentUser.get('first_name') + ' ' + currentUser.get('last_name')
            albumsCount = currentUser.get('counters').albums
            albumsDeclension = @declension albumsCount, ['альбом', 'альбома', 'альбомов']
            photosCount = currentUser.get('counters').photos
            @ui.userAlbumsCount.text albumsCount + ' ' + albumsDeclension + ', ' + photosCount + ' фото'
            @ui.userPic.attr('src', currentUser.get('photo_100')).attr('alt', @ui.userName.text()).css opacity: 1
            @ui.userData.css opacity: 1

            @ui.dropdownMyFriends.click =>
              AppRouter.navigate '/friends/', trigger: true
            @ui.dropdownMyGroups.click =>
              AppRouter.navigate '/groups/', trigger: true
            @ui.dropdownMyPhotos.click =>
              AppRouter.navigate 'albums' + currentUser.get('id'), trigger: true
            @ui.dropdownExit.click =>
              VK.Auth.logout ->
                console.log 'VK logout successful.'
        )

    declension: (number, titles) ->
      cases = [2, 0, 1, 1, 1, 2]
      `titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];`

  exports =
    ItemView: CurrentUserItemView