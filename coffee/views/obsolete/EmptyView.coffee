define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/empty-view.tpl'
], (Backbone, Marionette, _, EmptyViewTemplate) ->
  class EmptyViewItem extends Backbone.Marionette.ItemView
    template: _.template EmptyViewTemplate
    className: 'empty-view'

    initialize: (options) =>
      @model = new Backbone.Model().set
        errorText: options.errorText

  EmptyViewItem