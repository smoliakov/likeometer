define [
  '../../../js/backbone-min',
  'marionette',
  'underscore',
  'text!templates/image-item-group.tpl',
  'text!templates/content.tpl'
], (Backbone, Marionette, _, ImageItemTemplate, ContentTemplate) ->
  class GroupItemView extends Backbone.Marionette.ItemView
    template: _.template ImageItemTemplate
    className: 'cell'

  class GroupCollectionView extends Backbone.Marionette.CompositeView
    itemView: GroupItemView
    template: _.template ContentTemplate

  GroupCollectionView