// Generated by CoffeeScript 1.7.1
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['../../../js/backbone-min', 'marionette', 'underscore', 'text!templates/image-item-album.tpl', 'text!templates/content.tpl'], function(Backbone, Marionette, _, ImageItemTemplate, ContentTemplate) {
    var AlbumCollectionView, AlbumItemView, exports;
    AlbumItemView = (function(_super) {
      __extends(AlbumItemView, _super);

      function AlbumItemView() {
        this.initialize = __bind(this.initialize, this);
        return AlbumItemView.__super__.constructor.apply(this, arguments);
      }

      AlbumItemView.prototype.template = _.template(ImageItemTemplate);

      AlbumItemView.prototype.className = 'cell';

      AlbumItemView.prototype.initialize = function(options) {
        var id;
        id = this.model.get('id');
        switch (id) {
          case -15:
            id = '000';
            break;
          case -7:
            id = '00';
            break;
          case -6:
            id = '0';
        }
        return this.model.set({
          id: id
        });
      };

      return AlbumItemView;

    })(Backbone.Marionette.ItemView);
    AlbumCollectionView = (function(_super) {
      __extends(AlbumCollectionView, _super);

      function AlbumCollectionView() {
        return AlbumCollectionView.__super__.constructor.apply(this, arguments);
      }

      AlbumCollectionView.prototype.itemView = AlbumItemView;

      AlbumCollectionView.prototype.template = _.template(ContentTemplate);

      return AlbumCollectionView;

    })(Backbone.Marionette.CompositeView);
    return exports = {
      ItemView: AlbumItemView,
      CompositeView: AlbumCollectionView
    };
  });

}).call(this);
