define [
	'backbone'
	'marionette'
	'coffee/models/Photo'
	'text!templates/albums.tpl'
	'text!templates/albums-item.tpl'
	'text!templates/album.tpl'
	'text!templates/photo-item.tpl'
	'text!templates/groups-item.tpl'
	'text!templates/friend-item.tpl'
], (Backbone
    Marionette
		Photo
    AlbumsTemplate
    AlbumItemTeplate
		AlbumTemplate
		PhotoItemTemplate
		GroupsItemTemplate
		FriendItemTemplate
) ->
	class AlbumItem extends Backbone.Marionette.ItemView
		template: _.template AlbumItemTeplate
		tagName: 'li'
		attributes: =>
			style: "background-image: url(#{@model.get('src')})"

	class PhotoItem extends Backbone.Marionette.ItemView
		template: _.template PhotoItemTemplate
		tagName: 'li'
		attributes: =>
			style: "background-image: url(#{@model.get('src')})"

	class GroupsItem  extends Backbone.Marionette.ItemView
		template: _.template GroupsItemTemplate
		tagName: 'li'
		attributes: =>
			style: "background-image: url(#{@model.get('src')})"

	class FriendItem extends Backbone.Marionette.ItemView
		template: _.template FriendItemTemplate
		tagName: 'li'
		attributes:
			style: ''

	class Layout extends Backbone.Marionette.CompositeView
		itemViewContainer: '.collection'

		events:
			'click .user-likes': 'onUserLikesClick'
			'input input': 'onFriendsSearchInput'

		ui:
			userLikes: '.user-likes'
			input: 'input'

		initialize: =>
			@model.set 'page', @options.page

			@state =
				offset: 50
				count: 50

			@isMyLikesShown = false
			@originCollection = @options.collection.clone()
			@userLikesCollection = new Photo.Collection()

			firstSlice = 50

			switch @options.page
				when 'albums:user', 'albums:group'
					@template = _.template AlbumsTemplate
					@itemView = AlbumItem
					if @options.page is 'albums:group'
						@model.get('counters').albumsString = @model.get('counters').albums + ' ' + @albumsDeclension(@model.get('counters').albums)
						@model.get('counters').photosString = @model.get('counters').photos + ' ' + @photosDeclension(@model.get('counters').photos)
				when 'album'
					@template = _.template AlbumTemplate
					@itemView = PhotoItem
					@model.set 'size', @model.get('size') + ' ' + @photosDeclension @model.get('size')
					@model.set 'owner', @options.owner.toJSON()
					userLikes = 0
					@collection.each (item) ->
						if item.get('likes').user_likes
							userLikes++
					if userLikes
						userLikesString = "Мне #{@likesDeclension(userLikes)} #{userLikes} #{@photosDeclension(userLikes)}"
					else
						userLikesString = ''
					@model.set 'user_likes', userLikesString
				when 'groups'
					@template = _.template AlbumsTemplate
					@itemView = GroupsItem
				when 'friends'
					@template = _.template AlbumsTemplate
					@itemView = FriendItem
					firstSlice = 250
				else
					console.log 'Error: no such page.'

			@collection.reset @originCollection.slice 0, firstSlice

		declension: (number, titles) ->
			cases = [2, 0, 1, 1, 1, 2]
			`titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]]`

		photosDeclension: (number) ->
			@declension(number, ['фотография', 'фотографии', 'фотографий'])

		albumsDeclension: (number) ->
			@declension(number, ['альбом', 'альбома', 'альбомов'])

		likesDeclension: (number) ->
			@declension(number, ['понравилась', 'понравились', 'понравилось'])

		onUserLikesClick: =>
			if @isMyLikesShown
				# show all photos
				@collection.reset @originCollection.slice 0, 50
				@state.offset = 50
				@isMyLikesShown = false
				@ui.userLikes.text @model.get('user_likes')
			else
				# show my likes
				filteredByUserLike = @originCollection.filter (photo) ->
					photo.get('likes').user_likes
				@userLikesCollection = new Photo.Collection filteredByUserLike
				@collection.reset @userLikesCollection.slice 0, 50
				@state.offset = 50
				@isMyLikesShown = true
				@ui.userLikes.text 'Показать все фотографии'

		onBottomScroll: =>
			if @isMyLikesShown
				items = @userLikesCollection.slice @state.offset, @state.offset + @state.count
			else
				items = @originCollection.slice @state.offset, @state.offset + @state.count
			@state.offset = @state.offset + @state.count
			@collection.add items, sort: false

		onRender: =>
			$(window).bind 'scroll', =>
				if $(window).scrollTop() + $(window).height() > $(document).height() - 200
					@onBottomScroll()

		onShow: =>
			if @options.page in ['friends', 'albums:group', 'groups']
				@ui.input.focus()

		onBeforeClose: =>
			$(window).unbind 'scroll'

		onFriendsSearchInput: =>
			value = $.trim @ui.input.val()
			if value
				friends = @originCollection.filter (item) ->
					item.get('title').toLowerCase().indexOf(value) >= 0
				@collection.reset friends
				@state.offset = 50
			else
				@state.offset = 50
				slice = if @options.page is 'friends' then 250 else 50
				@collection.reset @originCollection.slice 0, slice

	Layout