// Generated by CoffeeScript 1.7.1
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['marionette', 'text!templates/error.tpl'], function(Marionette, ErrorTemplate) {
    var ErrorView;
    ErrorView = (function(_super) {
      __extends(ErrorView, _super);

      function ErrorView() {
        this.initialize = __bind(this.initialize, this);
        return ErrorView.__super__.constructor.apply(this, arguments);
      }

      ErrorView.prototype.className = 'text-center error';

      ErrorView.prototype.template = _.template(ErrorTemplate);

      ErrorView.prototype.initialize = function() {
        var defaults;
        defaults = {
          title: 'Упс, что-то пошло не так.',
          message: ''
        };
        return this.model = new Backbone.Model(_.extend(defaults, this.options));
      };

      return ErrorView;

    })(Backbone.Marionette.ItemView);
    return ErrorView;
  });

}).call(this);
