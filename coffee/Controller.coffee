define [
	'jquery'
	'underscore'
	'backbone'
	'marionette'
	'stellar'
	'coffee/models/Album'
	'coffee/models/Group'
	'coffee/models/Entity'
	'coffee/resources/users'
	'coffee/resources/photos'
	'coffee/resources/groups'
	'coffee/resources/friends'
	'coffee/views/Loading'
	'coffee/views/Layout'
	'coffee/views/Error'
	'coffee/views/Photo'
	'coffee/views/Landing'
	'coffee/views/History'
], ($
    _
    Backbone
    Marionette
		Stellar
    Album
    Group
    Entity
    users
    photos
    groups
    friends
    LoadingView
    LayoutView
    ErrorView
		PhotoView
		LandingView
		HistoryView
) ->
	class Controller extends Backbone.Marionette.Controller
		loadingView = new LoadingView()

#		checkVKAuth: (callback, error) =>
#			console.log '[auth] Check VK Auth'
#			VK.Auth.getLoginStatus (response) =>
#				if response.session
#					console.log '[auth] VK logged in already.'
#					callback()
#				else
#					VK.Auth.login (status)->
#						if status.session
#							console.log '[auth] VK login successful.'
#							callback()
#						else
#							console.log '[auth] VK login failed.'
#							error()
#					, window.grants

		index: =>
			VK.Auth.getLoginStatus (response) ->
				if response.session
					App.router.navigate '/albums', trigger: true
				else
					$('.app').addClass('hide')
					$('.app-landing').removeClass('hide')
					App.landingRegion.show new LandingView()
					$('.curtains').hide()

		friends: (friendId) =>
			$('.curtains').hide()
			friendId -= 0
			console.log 'Route: friends', friendId
			App.contentRegion.show loadingView

			userResource = new users()
			friendsResource = new friends()

			$.when(userResource.get(friendId), friendsResource.get(friendId))
			.done (user, friends) =>
				layoutView = new LayoutView
					model: user
					collection: friends
					page: 'friends'
				App.contentRegion.show layoutView
			.fail (error) =>
				console.log 'Error in friends', error
				App.contentRegion.show new ErrorView message: error

		groups: (userId) =>
			userId -= 0
			console.log 'Route: groups, user id', userId
			App.contentRegion.show loadingView

			userResource = new users()
			groupResource = new groups()

			$.when(userResource.get(userId), groupResource.get(userId))
			.done (user, groups) =>
				layoutView = new LayoutView
					model: user
					collection: groups
					page: 'groups'
				App.contentRegion.show layoutView
			.fail (error) ->
				console.log 'Groups get error', error

		albums: (ownerId) =>
			ownerId -= 0
			console.log 'Route: albums', ownerId
			App.contentRegion.show loadingView

			albumResource = new photos()
			userResource = new users()
			groupResource = new groups()

			if ownerId >= 0
				# user albums
				$.when(userResource.get(ownerId), albumResource.getAlbums(ownerId))
				.done (user, albums) =>
					unless user.get('deactivated')
						# remove empty albums
						albums = albums.filter (album) ->
							album.get('size') > 0
						albums = new Album.Collection albums
						# set albums counter
						user.get('counters').albums = albums.length
						layoutView = new LayoutView
							model: user
							collection: albums
							page: 'albums:user'
						App.contentRegion.show layoutView
					else
						# user is deactivated
						App.contentRegion.show new ErrorView message: 'Пользователь неактивен.'
				.fail (error) =>
					console.log error
					App.contentRegion.show new ErrorView message: 'Пользователь неактивен.'
			else
				# group albums
				groupDfd = groupResource.getById(ownerId)

				albumResource.getAlbums(ownerId)
				.done (albums) =>
					$.when(groupDfd)
					.done (group) =>
						@_albumsShow group, albums
					.fail (error) =>
						console.log 'Error in groups', error
						App.contentRegion.show new ErrorView message: error
				.fail =>
					console.log 'No regular albums, trying system albums.'
					albumResource.getSystemAlbums(ownerId)
					.done (albums) =>
						$.when(groupDfd)
						.done (group) =>
							@_albumsShow group, albums
						.fail (error) =>
							console.log 'Error in groups', error
							App.contentRegion.show new ErrorView message: error
					.fail (error) =>
						console.log 'Error in system albums', error
						App.contentRegion.show new ErrorView message: error

		_albumsShow: (group, albums) =>
			# remove empty albums
			albums = albums.filter (album) ->
				album.get('size') > 0
			albums = new Album.Collection albums

			# count photos in albums
			photosCount = albums.reduce (memo, album) ->
				memo + album.get('size')
			, 0

			# set counters
			group.get('counters').albums = albums.length
			group.get('counters').photos = photosCount

			# show view
			layoutView = new LayoutView
				model: group
				collection: albums
				page: 'albums:group'
			App.contentRegion.show layoutView


		album: (route) =>
			route = route.split '_'
			ownerId = route[0] - 0
			albumId = switch route[1]
				when '0' then -6
				when '00' then -7
				when '000' then -15
				else
					route[1] - 0
			console.log 'Route: album: user_id', ownerId - 0, 'album_id', albumId

			if App.contentRegion.currentView
				if App.contentRegion.currentView.model.get('page').indexOf('albums') < 0
					App.photoSlideRegion.reset()
					return
			else
				App.photoSlideRegion.reset()

			if App.photoSlideRegion.currentView
				# from viewing photo
				$.when(photosResource.getAlbums(ownerId, albumId), ownerResource)
				.done (album, owner) =>
					layoutView = new LayoutView
						model: album
						collection: App.photoSlideRegion.currentView.photos
						owner: owner
						page: 'album'
					App.contentRegion.show layoutView

					historyItem =
						link: album.get('link')
						title: album.get('title')
						src: album.get('sizes')[0].src
						time: new Date().getTime()
						owner: owner.get('title')
					App.likeHistory.add historyItem
				.fail (error) =>
					console.log error
					App.contentRegion.show new ErrorView message: error

				return

			App.contentRegion.show loadingView

			ownerResource = if ownerId > 0 then (new users()).get(ownerId) else (new groups()).getById(-ownerId)
			photosResource = new photos()

			$.when(photosResource.get(ownerId, albumId), photosResource.getAlbums(ownerId, albumId), ownerResource)
			.done (photos, album, owner) =>
				layoutView = new LayoutView
					model: album
					collection: photos
					owner: owner
					page: 'album'
				App.contentRegion.show layoutView

				historyItem =
					link: album.get('link')
					title: album.get('title')
					src: album.get('sizes')[0].src
					time: new Date().getTime()
					owner: owner.get('title')
				App.likeHistory.add historyItem
			.fail (error) =>
				console.log error
				App.contentRegion.show new ErrorView message: error

		errorRoute: (path) =>
			console.log 'Route: error', path
			App.contentRegion.show new ErrorView message: 'Неверный адрес.'

		photo: (route) =>
			route = route.split '_'
			ownerId = route[0] - 0
			photoId = route[1] - 0
			console.log 'Route: photo: owner_id', ownerId, 'photo_id', photoId

			$('body').addClass('photo-open')

			if App.photoSlideRegion.currentView
				App.photoSlideRegion.currentView.state.set
					ownerId: ownerId
					photoId: photoId
			else
				if App.contentRegion.currentView
					# try to get collection from already loaded resources
					if App.contentRegion.currentView.isMyLikesShown
						collection = App.contentRegion.currentView.userLikesCollection
					else
						collection = App.contentRegion.currentView.originCollection

				photoView = new PhotoView
					ownerId: ownerId
					photoId: photoId
					collection: collection
				App.photoSlideRegion.show photoView

		title: (title) =>
			window.document.title = title

		logout: =>
			VK.Auth.logout (r) ->
				unless r.session
					App.CURRENT_USER_ID = 0
					App.router.navigate '/', trigger: true
					$.stellar()

		login: =>
			VK.Api.call 'users.get', {fields: 'photo_50', v: 5.21}, (r) ->
				$('.app').removeClass('hide')
				$('.app-landing').addClass('hide')

				App.CURRENT_USER_ID = r.response[0].id

				user = r.response[0]

				$('.user .tab img').attr 'src', user.photo_50
				$('.user .tab span').text user.first_name + ' ' + user.last_name

				historyView = new HistoryView()
				App.asideContentRegion.show historyView
				App.likeHistory = historyView

				App.router.navigate '/albums', trigger: true

	Controller