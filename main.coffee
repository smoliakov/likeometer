require.config
	paths:
		jquery: 'vendor/jquery-1.11.1.min'
		underscore: 'vendor/underscore-1.6.0.min'
		backbone: 'vendor/backbone-1.1.2.min'
		marionette: 'vendor/backbone.marionette-1.8.5.min'
		vk: 'http://vk.com/js/api/openapi'
		app: 'coffee/Application'
		text: 'vendor/requirejs.text-2.0.10'
		stellar: 'vendor/jquery.stellar.min'
	shim:
		marionette:
			deps: ["backbone"]
			exports: "Marionette"

require ['jquery', 'app'], ($, App) ->
	$(document).ready ->
		console.log 'DOM ready.'

		window.apiId = 3188729
		window.grants = 262150 # photos, groups, friends

		VK.init apiId: window.apiId

		App.start()