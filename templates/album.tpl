<header>
	<div class="thumb" style="background-image: url(<%= sizes[1].src %>)"></div>
	<div class="title"><%- title %></div>
	<div class="subtitle">
		<a href="/<%= owner.link.indexOf(App.CURRENT_USER_ID) >= 0 ? 'albums' : owner.link %>"><%- owner.title %></a>
		<span class="dot">&bull;</span>
		<span class="items-count"><%= size %></span>
		<span class="dot<%= user_likes ? '' : ' hide' %>">&bull;</span>
		<a class="user-likes"><%= user_likes %></a>
	</div>
</header>

<ul class="collection"></ul>