<a href="/<%= link %>">
	<div class="title" title="<%- title %>"><span><%- title %></span></div>
	<div class="counter">
		<% if (size) { %>
		<i class="fa fa-picture-o"><%= size %></i>
		<% } %>
	</div>
</a>