<a href="/photo<%= owner_id %>_<%= id %>">
	<div class="photo counter">
		<i class="fa fa-heart left<%= likes.user_likes ? ' gold-before' : '' %>"><%= likes.count %></i>
		<i class="fa fa-comments right"><%= comments.count %></i>
	</div>
</a>