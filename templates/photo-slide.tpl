<div class="small-logo"></div>
<div class="counter"></div>
<div class="close"><i class="fa fa-times"></i></div>
<div class="previous"></div>
<div class="next"></div>
<div class="likes">
	<a data-bypass download target="_blank" class="download" title="Скачать"><i class="fa fa-cloud-download"></i></a>
	<a data-bypass target="_blank" class="external" title="Открыть ВКонтакте"><i class="fa fa-external-link"></i></a>
	<i class="fa fa-heart"></i>
</div>
<div class="photo-wrapper">
	<img src="" id="photo-slide">
</div>