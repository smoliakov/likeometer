<script type="text/javascript">
	// Resize main slide on window resize
	$(window).resize(function () {
		$('.hello').css('height', $(window).height());
	}).trigger('resize');

	// VK login
	$('.login-button').click(function () {
		VK.Auth.login(function (r) {
			if (r.session) {
				App.router.navigate('/login', {trigger: true});
			}
		}, 262150);
	});

	// RANDOM IMAGE
	$('.landing').css('background-image', 'url(/images/landing/image7.jpg)');

	// PARALLAX BG
	$.stellar();

	// SHARE
	var url = 'http://like-o-meter.ru';
	var title = 'Likeometer - новый взгляд на альбомы ВКонтакте';
	var description = 'Все фотографии отсортированы по количеству отметок "Мне нравится".';
	var image = 'http://like-o-meter.ru/images/likeometer.jpg';

	// VK.com
	var link_vk = 'https://vk.com/share.php?';
	link_vk += '&url=' + url;
	link_vk += '&title=' + title;
	link_vk += '&description=' + description;
	link_vk += '&image=' + image;
	$('#share-vk').click(function () {
		window.open(link_vk, '', '300,150');
	});

	// TWITTER.com
	var link_twitter = 'https://twitter.com/share?';
	link_twitter += '&text=' + title + ' @likeometer';
	link_twitter += '&url=' + url;
	$('#share-twitter').click(function () {
		window.open(link_twitter, '', '300,150');
	});

	// FACEBOOK.com
	var link_facebook = 'http://www.facebook.com/sharer/sharer.php?s=100';
	link_facebook += '&p[url]=' + url;
	link_facebook += '&p[images][0]=' + image;
	link_facebook += '&p[title]=' + title;
	$('#share-facebook').click(function () {
		window.open(link_facebook, '', '300,150');
	});

	// GOOGLE.com
	var link_google = 'https://plus.google.com/share?url=' + url;
	$('#share-google').click(function () {
		window.open(link_google, '', '300,150');
	});
</script>

<div class="hello">
	<div class="logo">
		<div><i class="fa fa-heart"></i>likeometer</div>
		<div class="moto">Новый взгляд на альбомы ВКонтакте</div>
	</div>
	<div class="login-pane">
		<span class="login-button">Войти <i class="fa fa-vk"></i></span>
	</div>
	<div class="news-pane">
		<a href="https://twitter.com/likeometer" data-bypass target="_blank"><i class="fa fa-twitter"></i></a>
		<a href="https://vk.com/like.o.meter" data-bypass target="_blank"><i class="fa fa-vk"></i></a>
	</div>
</div>

<div class="text-block">
	<div class="text-header"><i class="fa fa-heart"></i><b>likeometer</b> &mdash; это удобно</div>
	<div class="text-description">
		Ваши любимые фотографии, фотографии друзей и групп<br/>отсортированы по количеству отметок "Мне нравится".<br/>
		Смотрите только лучшее.
	</div>
</div>

<div class="screens">
	<div class="image-src" style="background-image: url(/images/screen1-ps.jpg)"></div>
	<div class="image-description">Фотографии альбома</div>

	<div class="image-src" style="background-image: url(/images/screen2-ps.jpg)"></div>
	<div class="image-description">Альбомы группы</div>

	<div class="image-src" style="background-image: url(/images/screen3-ps.jpg)"></div>
	<div class="image-description">Друзья</div>
</div>

<div class="footer">
	<div class="columns-6">
		<img src="/images/likeometer-black.png" alt="Likeometer" class="footer-logo"/>

		<div class="moto">Новый взгляд на альбомы ВКонтакте</div>
		<section class="share">
			<div id="share-vk"><i class="fa fa-vk"></i></div>
			<div id="share-twitter"><i class="fa fa-twitter"></i></div>
			<div id="share-facebook"><i class="fa fa-facebook"></i></div>
			<div id="share-google"><i class="fa fa-google-plus"></i></div>
		</section>
		<div class="copy">
			likeometer &copy; 2014, <a data-bypass href="http://meander-studio.ru" target="_blank">Meander Studio</a>
		</div>
	</div>
</div>