<div class="thumb-image" style="background-image: url(<%= src %>)"></div>
<div class="title" title="<%- title %>"><%- title %></div>
<div class="owner" title="<%- owner %>"><%- owner %></div>
