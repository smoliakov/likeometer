<a href="/<%= link %>">
	<div class="title" title="<%- title %>"><span><%- title %></span></div>
	<div class="counter">
		<i class="fa fa-camera"><%= size %></i>
	</div>
</a>