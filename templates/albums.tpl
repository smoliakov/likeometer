<header>
	<div class="thumb" style="background-image: url(<%= photo_100 %>)"></div>
	<div class="title"><%- title %></div>
	<div class="subtitle">
	<% if (page === 'albums:user' || page === 'groups' || page === 'friends') { %>
		<a href="/albums<%= id == App.CURRENT_USER_ID ? '' : id %>">Альбомы (<%= counters.albums %>)</a>
		<span class="dot">&bull;</span>
		<a href="/friends<%= id == App.CURRENT_USER_ID ? '' : id %>">Друзья (<%= counters.friends %>)</a>
		<% if (counters.groups) { %>
		<span class="dot">&bull;</span>
		<a href="/groups<%= id == App.CURRENT_USER_ID ? '' : id %>">Группы (<%= counters.groups %>)</a>
		<% } %>
	<% } else { %>
		<span><%= counters.albumsString %></span>
		<span class="dot">&bull;</span>
		<span><%= counters.photosString %></span>
	<% } %>
	</div>
</header>

<% if (page == 'friends' || page == 'albums:group' || page == 'groups') { %>
<input type="text" class="search">
<% } %>

<ul class="collection<%= page == 'friends' ? ' friends' : '' %>"></ul>