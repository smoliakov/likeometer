<div class="title">
    <div class="thumb">
        <img src="<%= thumb_src %>">
    </div>
    <div class="titleText"><%= _.escape(title) %><span class="count"><%= size %></span></div>
    <!--<div class="options">-->
        <!--<div class="sort">-->
            <!--<div class="dropdown-list">-->
                <!--<ul>-->
                    <!--<li id="sortByDate"><span class="calendar"></span>по дате</li>-->
                    <!--<li id="sortByLikes"><span class="likes"></span>по лайкам</li>-->
                    <!--<li id="sortByComments"><span class="comments"></span>по комментариям</li>-->
                <!--</ul>-->
            <!--</div>-->
        <!--</div>-->
        <!--<div class="filter">-->
            <!--<div class="dropdown-list">-->
                <!--<ul>-->
                    <!--<li id="filterAllPhotos"><span class="photos-count"></span></li>-->
                    <!--<li id="filterUserLikes"><span class="user-likes"></span></li>-->
                    <!--<li id="filterLikes"><span class="likes"></span></li>-->
                    <!--<li id="filterComments"><span class="comments"></span></li>-->
                <!--</ul>-->
            <!--</div>-->
        <!--</div>-->
    <!--</div>-->
</div>