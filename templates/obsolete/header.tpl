<div class="logo">
    <a href="/"><img src="/images/like-o-meter.png" width="176" height="40" alt="Like-o-meter"></a>
</div>
<div id="previous"></div>
<div id="userpic-and-dropdown">
    <img src="" width="30" height="30" class="user-pic" id="userPic">
    <div class="dropdown-list dropdown-header">
        <div class="triangle-up"></div>
        <ul>
            <li id="dropdown-my-photos">Мои альбомы</li>
            <li id="dropdown-my-friends">Мои друзья</li>
            <li id="dropdown-my-groups">Мои группы</li>
            <li id="dropdown-exit">Выход</li>
        </ul>
    </div>
</div>

<div class="user-data">
    <div class="user-name" id="userName"></div>
    <div class="user-albums-count" id="userAlbumsCount"></div>
</div>