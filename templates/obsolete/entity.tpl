<!-- Entity title -->
<div class="title">
    <% if (thumb) { %>
    <!-- Entity thumb -->
    <div class="thumb">
        <img src="<%= thumb %>">
    </div>
    <% } %>
    <!-- Entity title -->
    <div class="titleText <% if (!thumb) { print('no-left-margin'); } %>">
        <%= _.escape(title) %><span class="count"><%= size %></span>
    </div>
</div>
<!-- Entity items -->
<div class="items"></div>