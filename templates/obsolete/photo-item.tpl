<div class="inner-cell">
    <a href="photo<%= owner_id %>_<%= id %>"><img src="<%= photo_130 %>" class="remote-image"></a>
    <div class="caption">
        <div class="<%
            if (likes.user_likes == 1) {
                print('likes-gold');
            }
            else {
                print('likes');
            }
        %>"><%= likes.count %></div>
        <div class="comments"><%= comments.count %></div>
    </div>
</div>