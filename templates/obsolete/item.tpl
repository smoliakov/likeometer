<div class="inner-cell">
    <a href="/<%= link %>"><img src="<%= src %>" class="remote-image"></a>
    <div class="caption" title="<%= _.escape(title) %>"><%= _.escape(title) %></div>
</div>