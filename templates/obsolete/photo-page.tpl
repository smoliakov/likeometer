<div class="photo-header">
    <div class="logo"><img src="../../images/like-o-meter.png" width="176" height="40" alt="Like-o-meter"></div>
    <div class="close">&times;</div>
    <div class="count"></div>
</div>

<div class="image-wrapper">
    <div class="loader"><img src="https://vk.com/images/upload_inv_mono.gif"></div>
    <img id="currentImage" src="">

    <div class="left-arrow-wrapper">
        <div class="left-arrow"></div>
    </div>
    <div class="right-arrow-wrapper">
        <div class="right-arrow"></div>
    </div>
</div>

<div class="description">
    <div class="photo-likes"></div>
    <div class="photo-comments"></div>
    <div class="text"></div>
    <!--<div class="rotate-left"></div>-->
    <!--<div class="rotate-right"></div>-->
    <!--<div class="make-cover"></div>-->
    <div class="download"><a href="" download data-bypass target="_blank"><i class="fa fa-cloud-download"></i></a></div>
</div>
