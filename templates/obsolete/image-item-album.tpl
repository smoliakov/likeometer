<div class="inner-cell">
    <a href="/album<%= owner_id %>_<%= id %>"><img src="<%= thumb_src %>" class="remote-image"></a>
    <div class="caption" title="<%= _.escape(title) %>"><%= _.escape(title) %></div>
</div>